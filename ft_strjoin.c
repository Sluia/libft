#include "libft.h"

char *ft_strjoin(char const *str1, char const *str2)
{
	char *join;
	size_t len;

	len = ft_strlen(str1) + ft_strlen(str2);
	join = ft_strdup(str1, len);
	if (join == NULL)
	{
		return (NULL);
	}
	join = ft_strcat(join, str2, ft_strlen(str2));
	return (join);
}
