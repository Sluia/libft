#ifndef LIBFT_H

#define LIBFT_H

#include <stdlib.h>
#include <unistd.h>

void *ft_calloc(size_t nb_elem, size_t size);
int ft_memcmp(const void *ptr1, const void *ptr2, size_t n);
void *ft_memmove(void *dest, const void *src, size_t n);
void *ft_memset(void *ptr, int c, size_t n);
void *ft_memsrh(const void *ptr, int c, size_t n);

int ft_atoi(const char *str);
char *ft_itoa(int nb);
char **ft_split(char const *str, char delim); /////
char *ft_strcat(char *dest, const char *src, size_t n);
size_t ft_strcat_i(char *dest, const char *src, size_t n);
int ft_strcmp(const char *str1, const char *str2, size_t n);
char *ft_strcpy(char *dest, const char *src, size_t n);
size_t ft_strcpy_i(char *dest, const char *src, size_t n);
char *ft_strdup(char const *src, size_t n);
char *ft_strjoin(char const *str1, char const *str2);
size_t ft_strlen(const char *str);
char *ft_strsrh(const char *str, int c);
char *ft_strsrh_l(const char *str, int c);
char *ft_strstr(const char *str, const char *to_find);
char *ft_strtrim(char const *str, char const *set); /////
char *ft_substr(char const *str, unsigned int start, size_t len);

int ft_isalnum(int c);
int ft_isalpha(int c);
int ft_isascii(int c);
int ft_isdigit(int c);
int ft_isprint(int c);
int ft_tolower(int c);
int ft_toupper(int c);

void ft_wrchar(char c, int fd);
void ft_wrnbr(int nb, int fd);
void ft_wrstr(char *str, int fd);
void ft_wrstr_nl(char *str, int fd);

#endif
