// A travailler !

#include "libft.h"

size_t	ft_strtrim_start(char const *str, char const *set, size_t str_len)
{
	size_t i;

	i = 0;
	while (i < str_len)
	{
		if (!(ft_strsrh(set, str[i])))
		{
			return (i);
		}
		i++;
	}
	return (i);
}

size_t	ft_strtrim_end(char const *str, char const *set, size_t str_len)
{
	size_t i;

	i = 0;
	while (i < str_len)
	{
		if (!(ft_strsrh(set, str[str_len - i - 1])))
		{
			return (str_len - i);
		}
		i++;
	}
	return (str_len - i);
}

char	*ft_strtrim(char const *str, char const *set)
{
	char	*str_trim;
	size_t	str_len;
	size_t	i_start;
	size_t	i_end;

	if (str == NULL)
		return (NULL);
	str_len = ft_strlen(str);
	if (set == NULL)
		return (ft_strdup(str, str_len));
	i_start = ft_strtrim_start(str, set, str_len);
	i_end = ft_strtrim_end(str, set, str_len);
	if (i_start >= i_end)
	{
		return (ft_strdup("", 0));
	}
	str_trim = ft_strdup(str, i_end - i_start);
	if (str_trim == NULL)
	{
		return (NULL);
	}
	return (str_trim);
}
