#include "libft.h"

char *ft_substr(char const *str, unsigned int start, size_t len)
{
	char *substr;

	if (str == NULL || ft_strlen(str) < start)
	{
		return (NULL);
	}
	substr = ft_strdup(&str[start], len);
	if (substr == NULL)
	{
		return (NULL);
	}
	return (substr);
}
