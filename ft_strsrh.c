#include "libft.h"

char *ft_strsrh(const char *str, int c)
{
	size_t len;
	size_t i;

	len = ft_strlen(str) + 1;
	i = 0;
	while (i < len)
	{
		if (c == str[i])
		{
			return ((char *)&str[i]);
		}
		i++;
	}
	return (NULL);
}
