#include "libft.h"

void ft_wrnbr(int nb, int fd)
{
	long l_nb;

	l_nb = nb;
	if (l_nb < 0)
	{
		ft_wrchar('-', fd);
		l_nb *= -1;
	}
	if (l_nb > 9)
	{
		ft_wrnbr(l_nb / 10, fd);
		ft_wrchar((l_nb % 10) + 48, fd);
	}
	else
	{
		ft_wrchar(l_nb + 48, fd);
	}
}