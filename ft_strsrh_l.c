#include "libft.h"

char *ft_strsrh_l(const char *str, int c)
{
	size_t	length;
	size_t	i;
	char	*last_occ;

	length = ft_strlen(str) + 1;
	i = 0;
	last_occ = NULL;
	while (i < length)
	{
		if (c == str[i])
		{
			last_occ = (char *)&str[i];
		}
		i++;
	}
	return (last_occ);
}
