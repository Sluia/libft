#include "libft.h"

void *ft_memset(void *ptr, int c, size_t n)
{
	unsigned char *ptr_c;
	size_t i;

	ptr_c = (unsigned char *)ptr;
	i = 0;
	while (i < n)
	{
		ptr_c[i] = (unsigned char)c;
		i++;
	}
	return (ptr);
}
