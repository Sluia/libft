#include "libft.h"

char *ft_strstr(const char *str, const char *to_find)
{
	size_t to_find_len;
	size_t i;

	if (to_find[0] == '\0')
	{
		return ((char *)str);
	}
	to_find_len = ft_strlen(to_find);
	i = 0;
	while (str[i])
	{
		if ((ft_strcmp(&str[i], to_find, to_find_len)) == 0)
		{
			return ((char *)&str[i]);
		}
		i++;
	}
	return (NULL);
}
