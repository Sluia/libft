#include "libft.h"

void *ft_memsrh(const void *ptr, int c, size_t n)
{
	unsigned char *ptr_c;
	size_t i;

	ptr_c = (unsigned char *)ptr;
	i = 0;
	while (i < n)
	{
		if ((unsigned char)c == ptr_c[i])
		{
			return ((void *)&ptr_c[i]);
		}
		i++;
	}
	return (NULL);
}
