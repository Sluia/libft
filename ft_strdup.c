#include "libft.h"

char *ft_strdup(char const *src, size_t n)
{
	char *dup;

	dup = ft_calloc(n + 1, sizeof(char));
	if (dup == NULL)
	{
		return (NULL);
	}
	dup = ft_strcpy(dup, src, n);
	return (dup);
}
