NAME = libft.a

SRC =	ft_atoi.c \
		ft_calloc.c \
		ft_isalnum.c \
		ft_isalpha.c \
		ft_isascii.c \
		ft_isdigit.c \
		ft_isprint.c \
		ft_itoa.c \
		ft_memcmp.c \
		ft_memmove.c \
		ft_memset.c \
		ft_memsrh.c \
		ft_split.c \
		ft_strcat.c \
		ft_strcat_i.c \
		ft_strcmp.c \
		ft_strcpy.c \
		ft_strcpy_i.c \
		ft_strdup.c \
		ft_strjoin.c \
		ft_strlen.c \
		ft_strsrh.c \
		ft_strsrh_l.c \
		ft_strstr.c \
		ft_strtrim.c \
		ft_substr.c \
		ft_tolower.c \
		ft_toupper.c \
		ft_wrchar.c \
		ft_wrnbr.c \
		ft_wrstr.c \
		ft_wrstr_nl.c

OBJ = ${SRC:.c=.o}

BONUS = ft_lstadd_back.c ft_lstadd_front.c ft_lstlast.c ft_lstnew.c \
	ft_lstsize.c ft_lstdelone.c ft_lstclear.c ft_lstmap.c ft_lstiter.c

BONUS_OBJ = ${BONUS:.c=.o}

CC = gcc ${CFLAGS}

CFLAGS = -Wall -Werror -Wextra

RM = rm -f

.c.o:
	${CC} -c $< -o ${<:.c=.o}

$(NAME): ${OBJ}
	ar rcs ${NAME} ${OBJ}

all: ${NAME}

bonus: ${OBJ} ${BONUS_OBJ}
	ar rcs ${NAME} ${OBJ} ${BONUS_OBJ}

clean:
	${RM} ${OBJ} ${BONUS_OBJ}

fclean: clean
	${RM} ${NAME}

re: fclean all

.PHONY: all bonus clean fclean re
